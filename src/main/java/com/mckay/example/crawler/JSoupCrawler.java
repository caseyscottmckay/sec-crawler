package com.mckay.example.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * JSoupCrawler Crawls Sec Gov Site to pull company subsidiaries based on
 *  Tested in ubuntu 1604 and Windows 8
 *  -MAVEN Instructions:
 *      1. in main directory (mc-crawler) run the following command: mvn clean install
 *      2. after sucessful install, run the following command verbatim to run the crawler (including quotation marks): mvn exec:java -Dexec.args="875045,318154,320193,816284,804055,882095,875320"
 *      3. output is one cik number with company subsidiaries per line in json format
 * @author Casey Scott McKay
 */
public class JSoupCrawler {

    public static void main(String... args) throws IOException {

        if (args.length != 1) {
            System.err.println("Missing cikNumbers. Please enter 1 or more CIK Numbers (e.g., 875045) separated by commas:");
            return;
        }

        String temp = "875045,318154,320193,816284,804055,882095,875320,80405565444444444,882095sadf,875320999";
        processAllNumbers(args[0]);
    }




    public static void processAllNumbers(String cikNumbersString) throws IOException {
        String[] cikNumbers=cikNumbersString.split(",");
        for(String s:cikNumbers) {
            //filter out all non-cik-confmring strings,, e.g., no letters, <10 digits
            if (s.matches(".*[^0-9]+.*")||s.length()>10) {
                System.out.println("CIK Number "+s+" invalid. No letters. Numbers only. Less than ten digits (e.g., 804055)");
            }
            //next check validity of cik number by checking whether number returns search results from sex site
            //if valid --> process the number to get subsidiaries
            //todo  many ways to do this, so may change later
            else {
                int cikNumTemp = Integer.parseInt(s);
                checkCIKValdity(cikNumTemp);
                if (checkCIKValdity(cikNumTemp) == false) {
                    System.out.println("CIK number invalid--no results found for this number: " + s);
                } else processCIKNumber(cikNumTemp);
            }
        }
    }

    public static String processCIKNumber(int inputCikNumber) throws IOException {

        String urlTemp= processPage("https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK="+inputCikNumber+"&type=10-K")+"\n";
        String jsonOut="";
        String[] current10KUrls=urlTemp.split("\n");
        String urlToEx21="https://www.sec.gov/"+urlTemp;
        HashMap<String, String> subsidiaryMap=getJsonObjectFromUrl(urlToEx21);
        jsonOut="{\""+inputCikNumber+"\": [";
        String kTemp="";
        for(Map.Entry e: subsidiaryMap.entrySet()){
            kTemp=e.getKey().toString();
                //replace all weird encoded double quotes with single quotes to be json friendly (alternatively, could replace with unicode double quotes and escape, but not necessary as none of these are quoted material)
            //todo figure out a better way to do this--this cannot be proper?
            kTemp=kTemp.replaceAll("","'").replaceAll("","'");
            jsonOut+="\""+kTemp+"\", ";
            }
        jsonOut=jsonOut.trim();
        jsonOut=jsonOut.substring(0,jsonOut.length()-1);
        jsonOut+="]}";
        System.out.println(jsonOut);

        return jsonOut;

    }

    /*
        --Method checks individual input numbers to see whether the number is a valid cik number
        --CIK number is all numbers up to 10 digits
        --Bad number = "No matching CIK." on results page
        @params int cikNum-- the CIK number to check
        @returns boolean--true if CIK number is valid or false if invalid
     */
    public static boolean checkCIKValdity(int cikNum) throws IOException {
        String cikUrlToCheck="https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK="+cikNum+"&type=10-K";
        Document doc = Jsoup.connect(cikUrlToCheck).get();

        if(doc.text().contains("No matching CIK.")){
            return false;
        }
        else {
            return true;
        }

    }

    public static String processPage(String URL) throws  IOException{
        //check if the given URL is already in database
        String urlOut="";
        //get useful information
        Document doc = Jsoup.connect(URL).get();

        //get all links and recursively call the processPage method
        Elements links = doc.select("a[href]");
        //to check for first link in fee, which is most up to date
        int i = 0;
        for (Element link : links) {
            if (link.attr("href").contains("sec.gov"))
                processPage(link.attr("abs:href"));
            //processPage(link.attr(""));
            String tempLink = link.toString();
            if (tempLink.contains("Documents</a>") && i == 0) {
                i++;
                urlOut = tempLink.substring(tempLink.indexOf("/Archives"));
                //todo:investigate weird thing here won't catch index\\.htm but catches +9 for some reason?
                urlOut = urlOut.substring(0, urlOut.indexOf("index.htm") + 9);
            }
        }
        return urlOut;
    }


    /*
        --Method used by processCIKs (See above).
        --Method processes a sex url recursively to seek out the latest 10k
        --latest 10k is always first in feed, so use simple i checker to get 10k link
        --returns the proper url for the most current 10k link
        @Params: url for a specific company's 10k page
        @Returns: url for latest 10k
     */

    public static HashMap<String, String>  getJsonObjectFromUrl(String urlToStartWith) {
        Document doc;
        HashMap<String, String> subsidCompanyNamesAndStates=new HashMap();
        String name = "";
        String state = "";
        try {
            // need http protocol
            doc = Jsoup.connect(urlToStartWith).get();
            //get all links so we can find exhbit 21
            Elements links = doc.select("a[href]");
            for (Element link : links) {
                // get the value from href attribute
                //todo this covers all test cases but probably not all cases, so investigate this
                if (link.attr("href").matches(".*(exhibit|ex)21.*?\\.htm")) {
                    String ex21URL = "https://www.sec.gov" + (link.attr("href"));
                    Document doc21 = Jsoup.connect(ex21URL).get();
                    String html = doc21.html();
                    //custom method to catch #7 that refuses to use a table for the data
                    //todo need better way to get outliers--i.e., this misses anything that is not div or table (and may be messy for other divs outside custom
                    if ((!html.contains("<tr>") && (!html.contains("<td>")))) {
                        for (Element row : doc21.select("div")) {
                            if(row.text().contains(",")) {
                                name = row.text().substring(0, row.text().lastIndexOf(","));
                                if(name.length()>2&&(!name.contains("SUBSIDIAR*"))&&(!name.contains("Name of Subsidiary"))&&(!name.contains("a subsidiary of"))&&(!name.equals(name.toUpperCase()))&&(!name.equals("name"))){
                                    subsidCompanyNamesAndStates.put(name, name);
                                }
                                state  = row.text().substring(row.text().lastIndexOf(","));
                            }
                        }
                    }
                    if ((html.contains("<tr>") && (html.contains("<td>")))) {
                        for (Element table : doc21.select("table")) {
                            for (Element row : table.select("tr")) {
                                Elements tds = row.select("td");
                                name = "";
                                state = "";
                                String temp = "";
                                for (int j = 0; j < tds.size(); j++) {
                                    temp += (tds.get(j).text() + "#");
                                    String[] tempOps=temp.split("#");
                                    if(tempOps.length>0){
                                        name=tempOps[0];
                                        //jsonObject.put("Name", name);
                                        //jsonObject.put("Location", state);
                                        //list.add(jsonObject);
                                        if(name.length()>2&&(!name.contains("SUBSIDIAR*"))&&(!name.contains("Name of Subsidiary"))&&(!name.contains("a subsidiary of"))&&(!name.equals(name.toUpperCase()))&&(!name.equals("name"))){
                                            subsidCompanyNamesAndStates.put(name, name);
                                        }

                                    }
                                }
                            }
                        }
                    }
                    else {
                        for (Element table : doc21.select("table")) {
                            for (Element row : table.select("tr")) {
                                Elements tds = row.select("td");
                                String temp = "";
                                for (int j = 0; j < tds.size(); j++) {
                                    temp += (tds.get(j).text() + "#");
                                    String[] tempOps=temp.split("#");
                                    if(tempOps.length>0){
                                        name=tempOps[0];
                                        if(name.length()>2&&(!name.contains("SUBSIDIAR*"))&&(!name.contains("Name of Subsidiary"))&&(!name.contains("a subsidiary of"))&&(!name.equals(name.toUpperCase()))&&(!name.equals("name"))){
                                            subsidCompanyNamesAndStates.put(name, name);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return subsidCompanyNamesAndStates;
    }
}
